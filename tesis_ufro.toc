\select@language {spanish}
\contentsline {chapter}{AGRADECIMIENTOS}{\es@scroman {i}}
\contentsline {chapter}{RESUMEN}{\es@scroman {iii}}
\contentsline {chapter}{\numberline {1}INTRODUCCI\IeC {\'O}N}{1}
\contentsline {section}{\numberline {1.1}Objetivo General}{3}
\contentsline {section}{\numberline {1.2}Objetivos Espec\IeC {\'\i }ficos}{3}
\contentsline {chapter}{\numberline {2} ANTECEDENTES GENERALES }{5}
\contentsline {section}{\numberline {2.1}Conocimientos B\IeC {\'a}sicos}{6}
\contentsline {subsection}{\numberline {2.1.1}Coprocesador Intel Xeon Phi}{6}
\contentsline {section}{\numberline {2.2}Programaci\IeC {\'o}n Intel Xeon Phi}{8}
\contentsline {subsection}{\numberline {2.2.1}OpenMP}{9}
\contentsline {subsection}{\numberline {2.2.2}Programaci\IeC {\'o}n Intel Xeon Phi v\IeC {\'\i }a offload }{10}
\contentsline {subsection}{\numberline {2.2.3}Offload vs. ejecuci\IeC {\'o}n nativa}{12}
\contentsline {subsection}{\numberline {2.2.4}Vectorizaci\IeC {\'o}n efectiva}{12}
\contentsline {subsection}{\numberline {2.2.5}Seis etapas de metodolog\IeC {\'\i }a de vectorizaci\IeC {\'o}n}{12}
\contentsline {section}{\numberline {2.3}Espacios m\IeC {\'e}tricos}{13}
\contentsline {subsection}{\numberline {2.3.1}B\IeC {\'u}squedas por similitud}{13}
\contentsline {subsection}{\numberline {2.3.2}Indexaci'on}{14}
\contentsline {subsection}{\numberline {2.3.3}\IeC {\'I}ndices m\IeC {\'e}tricos}{17}
\contentsline {subsubsection}{\numberline {2.3.3.1}Lista de Clusters (\emph {LC}) }{18}
\contentsline {section}{\numberline {2.4}Trabajo Relacionado}{20}
\contentsline {subsection}{\numberline {2.4.1}Trabajo relacionado en Sistemas multi-core}{20}
\contentsline {subsection}{\numberline {2.4.2}Trabajo Relacionado en GPU}{22}
\contentsline {chapter}{\numberline {3}\uppercase {Descripci\IeC {\'o}n de las actividades realizadas}}{25}
\contentsline {section}{\numberline {3.1}Materiales }{26}
\contentsline {section}{\numberline {3.2}Metodolog\IeC {\'\i }a }{27}
\contentsline {subsection}{\numberline {3.2.1} Hip\IeC {\'o}tesis}{27}
\contentsline {section}{\numberline {3.3}Objetivos }{27}
\contentsline {section}{\numberline {3.4}Metodolog\IeC {\'\i }a}{28}
\contentsline {section}{\numberline {3.5}Algoritmos de b\IeC {\'u}squeda por rango }{30}
\contentsline {subsection}{\numberline {3.5.1}B\IeC {\'u}squeda por rango exhaustiva }{31}
\contentsline {subsubsection}{\numberline {3.5.1.1}Algoritmo exhaustivo MIC v1 }{32}
\contentsline {subsubsection}{\numberline {3.5.1.2}Algoritmo exhaustivo MIC v2 }{34}
\contentsline {subsection}{\numberline {3.5.2}B\IeC {\'u}queda por rango indexada }{35}
\contentsline {subsubsection}{\numberline {3.5.2.1}Algoritmo basado en indexaci\IeC {\'o}n LC MIC DIST-Q }{36}
\contentsline {subsubsection}{\numberline {3.5.2.2} Algoritmo basado en indexaci\IeC {\'o}n LC MIC DIST-C }{38}
\contentsline {section}{\numberline {3.6}Algoritmos multi-core}{40}
\contentsline {chapter}{\numberline {4}RESULTADOS Y DISCUSI\IeC {\'O}N}{41}
\contentsline {section}{\numberline {4.1}Experimentos sobre algoritmos exhaustivos }{43}
\contentsline {section}{\numberline {4.2}Experimentos sobre algoritmos basados en indexaci\IeC {\'o}n }{45}
\contentsline {section}{\numberline {4.3}Experimentos entre versiones exhaustivas y basadas en indexaci\IeC {\'o}n }{47}
\contentsline {chapter}{\numberline {5}CONCLUSIONES}{49}
\contentsline {section}{\numberline {5.1}Contribuciones del Trabajo de T\IeC {\'\i }tulo}{51}
\contentsline {chapter}{BIBLIOGRAF\IeC {\'I}A}{53}
